from pygame import sprite, mixer


class BasicObject(sprite.Sprite):

    def __init__(self, x_position, y_position, image, status=""):
        super().__init__()
        self.x_position = x_position
        self.y_position = y_position
        self.image = image
        self.status = status

    @staticmethod
    def play_music(music_file):
        mixer.Sound(music_file).play()

    def draw(self, screen):
        self.image.set_colorkey((0, 0, 0))
        self.rect = self.image.get_rect()
        screen.blit(self.image, (self.x_position, self.y_position))

    def move(self):
        self.y_position -= 0.4
        if self.y_position < 0:
            self.status = "terminated"

    def check_status(self, screen, remove_table):
        self.draw(screen)
        self.move()
        if self.status == "terminated":
            remove_table.append(self)
        return remove_table

    def remove(self, i, list):
        if self.status == "terminated" and i in list:
            list.remove(i)
        return list
