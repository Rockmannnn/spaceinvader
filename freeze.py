from BasicObject import *
from pygame import time, display


class Freeze(BasicObject):
    def __init__(self, x_position, y_position, freeze_image, status):
        super().__init__(x_position, y_position, freeze_image, status)
        self.rect = self.image.get_rect()

    def move(self):
        self.y_position += 0.2
        if self.y_position > display.get_surface().get_height() - 96:
            self.status = "terminated"

    def check_collision(self, player, alien_list):
        if (self.x_position + 60 >= player.x_position >= self.x_position - 60) and (
                self.y_position + 70 >= player.y_position >= self.y_position - 70):
            self.status = "terminated"

            for i in alien_list:
                i.start_time = time.get_ticks()
