from bullet import *
from pygame import *


class Player(BasicObject):
    def __init__(self, x_position, y_position, player_image, level):
        super().__init__(x_position, y_position, player_image)
        self.level = level
        self.life_count = 3
        self.damage = 10
        self.width, self.length = self.image.get_size()
        self.start_time = None
        self.shoot_count = 0

    '''method for drawing the player ship on screen'''

    def draw(self, screen):
        self.image.set_colorkey((0, 0, 0))
        self.rect = self.image.get_rect()
        screen.blit(self.image, (self.x_position, self.y_position))
        self.image = image.load("Spaceship_images\\SpaceShip_lev" + str(self.level) + ".png")

    '''checking the position of the player on the board preventing from leaving the board'''

    def check_boundaries(self):
        if self.y_position > display.get_surface().get_height() - self.width:
            self.y_position = display.get_surface().get_height() - self.width
        if self.y_position <= 0:
            self.y_position = 0

        if self.x_position > display.get_surface().get_width():
            self.x_position = -50
        if self.x_position < -50:
            self.x_position = display.get_surface().get_width()

    '''method for moving the player on the board'''

    def move(self):
        keys = key.get_pressed()

        self.x_position += (keys[K_d] - keys[K_a]) * 0.3
        self.y_position += (keys[K_s] - keys[K_w]) * 0.3

        self.check_boundaries()

    '''shooting method for player'''

    def shoot(self, bullet_table):
        self.shoot_count += 1

        '''restricting the number of given shoots at a given time'''
        if self.shoot_count <= 5:
            '''determining the sort of bullet by level'''
            shooting_level = {
                1: [[34], [30]],
                2: [[28, 40], [30, 30]],
                3: [[30, 38], [30, 30]],
                4: [[19, 53], [30, 30]],
                5: [[38, 26, 50], [40, 25, 25]],
                6: [[38, 0, 71], [30, -5, -5]],
                7: [[20, 52], [20, 20]]
            }

            switching_shooting_par = shooting_level.get(self.level)
            for i in range(len(switching_shooting_par[0])):

                if self.level < 4:
                    bullet_image = image.load("Images\\lv1_bullet.png")
                    #self.play_music('Music\\shoot.wav')
                elif 4 <= self.level < 7:
                    bullet_image = image.load("Images\\lv2_bullet.png")
                    #self.play_music('Music\\heavy_shoot.wav')
                else:
                    bullet_image = image.load("Images\\lv3_bullet.png")
                    #self.play_music('Music\\explosion.wav')

                bullet_table.append(
                    Bullet(self.x_position + int(switching_shooting_par[0][i]),
                           self.y_position - int(switching_shooting_par[1][i]),
                           "", bullet_image))

        '''if maximum shoots are given in a time block shooting for a period of time'''
        if self.shoot_count == 5:
            self.start_time = time.get_ticks()
        self.stop_shooting()

        return bullet_table

    '''shooting method used in shoot method'''
    def stop_shooting(self):
        if self.start_time:
            time_since_enter = time.get_ticks() - self.start_time
            if time_since_enter > 1000:
                self.shoot_count = 0
                self.start_time = None

    '''checking collision between player and aliens on the board'''

    def check_collision(self, alien):
        width, height = alien.image.get_size()
        """if the player and alien have collided decrease the number of players lives"""
        if (self.x_position + width/4 >= alien.x_position >= self.x_position - width) and (
                self.y_position >= alien.y_position >= self.y_position - height):
            # self.play_music('Music\\explosion.wav')
            '''setting the player to the beginning position'''
            self.x_position = display.get_surface().get_width() / 2.1
            self.y_position = display.get_surface().get_height() / 1.2
            '''setting players data to level 1'''
            self.life_count -= 1
            self.level = 1
            self.damage = 10
