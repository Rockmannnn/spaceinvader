# SpaceInvader_project


## Basic information
Implementation of SpaceInvader game with the usage of a python library specified for gaming applications.\
Main task of the game is to destroy all alien within ten waves.\
Player loses the game if his spacecraft has been destroyed three times.\
Game has been implemented with the concept of Object-Oriented-Programming.
## Starting parameters:

match = 2\
mismatch = -2\
gap = -1


#  libraries used in application

python 3.9.5\
pygame 2.0.2\
multipledispatch 0.6.0
