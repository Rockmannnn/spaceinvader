from BasicObject import *


class Bullet(BasicObject):
    def __init__(self, x_position, y_position, status, bullet_image):
        super().__init__(x_position, y_position, bullet_image, status)
        self.rect = self.image.get_rect()

    def check_collision(self, alien, player):
        width, height = alien.image.get_size()
        if (self.x_position + width >= alien.x_position >= self.x_position - width) and (
                self.y_position >= alien.y_position >= self.y_position - height):
            self.status = "terminated"
            alien.life -= player.damage
            if alien.life <= 0:
                alien.status = "terminated"
