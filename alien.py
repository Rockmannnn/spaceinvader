from BasicObject import *
from multipledispatch import dispatch
from pygame import *
from operator import attrgetter


class Alien(BasicObject):
    def __init__(self, x_position, y_position, alien_image, direction, status, life):
        super().__init__(x_position, y_position, alien_image, status)
        self.direction = direction
        self.life = life
        self.width, self.length = self.image.get_size()
        self.start_time = None

    ''''koejka z head i tail'''

    @staticmethod
    def moving_sequence(alien_list):
        x_min = min(alien_list, key=attrgetter('x_position'))
        x_max = max(alien_list, key=attrgetter('x_position'))
        return x_max.x_position, x_min.x_position

    def check_boundaries(self, x_max, x_min, alien_list):
        if self.direction != "N":
            if x_max >= display.get_surface().get_width() - self.width:
                self.direction = "L"
                self.y_position += 10
            if x_min <= 0:
                self.direction = "R"
                self.y_position += 10

            if self.y_position > display.get_surface().get_height():
                self.y_position = -50

            if any(i.y_position <= 50 for i in alien_list):
                self.y_position += 0.3

            if self.direction == "R":
                self.x_position += 0.3
            if self.direction == "L":
                self.x_position -= 0.3

    @dispatch(object)
    def move_alien(self, alien_list):
        x_max, x_min = self.moving_sequence(alien_list)
        self.check_boundaries(x_max, x_min, alien_list)

    @dispatch(object, object, object)
    def check_status(self, screen, remove_table, alien_list):
        self.draw(screen)
        self.move_alien(alien_list)
        if self.status == "terminated":
            remove_table.append(self)
        return remove_table

    def pause(self):

        if self.start_time:
            time_since_enter = time.get_ticks() - self.start_time

            if time_since_enter < 3000:
                self.direction = "N"
            else:
                self.direction = "R"
                self.start_time = None
