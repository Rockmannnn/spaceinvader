from alien import *
from player import *
from level_up import *
from freeze import *
import random
import pygame

pygame.init()


class Game:
    def __init__(self, game_level, level_time):
        self.game_level = game_level
        self.level_time = level_time
        self.screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
        self.background = pygame.image.load("Images\\Background_1.jpeg")
        self.running = True
        self.clock = pygame.time.Clock()
        pygame.mouse.set_visible(False)

    @staticmethod
    def make_aliens(level):
        alien_list = []
        file = open('Levels\\lvl_' + str(level) + '.txt', 'r')
        for x in file:
            s = x.split(',')
            converted_list = []
            for element in s:
                converted_list.append(element.strip())
            alien_list.append(
                Alien(x_position=int(converted_list[0]), y_position=int(converted_list[1]),
                      alien_image=image.load(str(converted_list[2])), direction=str(converted_list[3]),
                      status=str(converted_list[4]), life=int(converted_list[5])))

        return alien_list

    @staticmethod
    def prepare_music():
        pygame.mixer.init()
        for i in range(2):
            if i == 0:
                pygame.mixer.music.load("Music\\Music_" + str(i) + ".mp3")
            else:
                pygame.mixer.music.queue("Music\\Music_" + str(i) + ".mp3")

    @staticmethod
    def play_music():
        pygame.mixer.music.play()

    @staticmethod
    def collision_detection(alien_list, bullet_table, level_up_table, freeze_table, player):
        for i in alien_list:
            player.check_collision(i)
            i.pause()
        for j in bullet_table:
            for k in alien_list:
                j.check_collision(k, player)
        for l in level_up_table:
            l.check_collision(player)
        for k in freeze_table:
            k.check_collision(player, alien_list)

        map(lambda x: x.check_collision(player), level_up_table)
        map(lambda x: x.check_collision(player, alien_list), freeze_table)

    @staticmethod
    def remove_object(bullet_table, alien_list, level_up_table, freeze_table, remove_table):

        for i in remove_table:
            i.remove(i, bullet_table)
        for j in remove_table:
            j.remove(j, alien_list)
        for k in remove_table:
            k.remove(k, level_up_table)
        for l in remove_table:
            l.remove(l, freeze_table)

    @staticmethod
    def make_random_upgrades(level_up_table, freeze_table):

        if random.randint(0, 1000000) <= 100:
            rand_x = random.randint(0, display.get_surface().get_width())
            level_up_table.append(LevelUp(rand_x, 0, image.load("Images\\LevelUp.png"), "live"))

        if random.randint(0, 2000000) <= 200:
            rand_x_1 = random.randint(0, display.get_surface().get_width())
            freeze_table.append(Freeze(rand_x_1, 0, image.load("Images\\Freeze.png"), "live"))

        return level_up_table, freeze_table

    def make_background(self):
        self.screen.blit(self.background, (0, 0))

    def communication_screen(self, text, text_color):

        if text == "V I C T O R Y":
            pygame.mixer.music.load("Music\\victory.mp3")
            self.play_music()

        pause = True
        text = pygame.font.Font('freesansbold.ttf', 62).render(str(text), True, text_color)
        position = text.get_rect()
        position.center = (display.get_surface().get_width() / 2, display.get_surface().get_height() / 2)

        while pause:
            for events in pygame.event.get():
                if events.type == pygame.KEYDOWN:
                    if events.key == K_ESCAPE:
                        pause = False
                        self.running = False
                    if events.key == pygame.K_p:
                        pause = False
            self.screen.blit(text, position)
            pygame.display.update()

    def pause_screen(self, text, text_color, start_time):

        if start_time:
            text = pygame.font.Font('freesansbold.ttf', 62).render(str(text), True, text_color)
            position = text.get_rect()
            position.center = (display.get_surface().get_width() / 2, display.get_surface().get_height() / 2)
            self.screen.blit(text, position)

        if start_time:
            time_since_enter = time.get_ticks() - start_time

            if time_since_enter < 3000:
                pass
            else:
                start_time = None
        return start_time

    def level_check(self, alien_list):
        if len(alien_list) == 0:
            self.game_level += 1
            self.level_time = time.get_ticks()
            if 6 <= self.game_level < 10:
                self.background = pygame.image.load("Images\\Background_2.jpg")
            if self.game_level >= 10:
                self.background = pygame.image.load("Images\\Background_3.jpeg")
            if self.game_level > 10:
                self.communication_screen("V I C T O R Y", (255, 255, 0))
            else:
                alien_list = self.make_aliens(self.game_level)
        return alien_list, self.game_level, self.level_time

    def loose_condition(self, player):
        if player.life_count == 0:
            self.communication_screen("Y O U  L O S T", (255, 255, 255))

    def check_events(self, player, bullet_table):
        # player.x_position, player.y_position = pygame.mouse.get_pos()
        for events in pygame.event.get():

            if events.type == KEYDOWN:
                if events.key == K_ESCAPE:
                    self.running = False
                if events.key == pygame.K_p:
                    pygame.mixer.music.pause()
                    self.communication_screen("P A U S E", (255, 255, 255))
                    pygame.mixer.music.unpause()
                if events.key == pygame.K_SPACE:
                    bullet_table = player.shoot(bullet_table)
            elif events.type == pygame.MOUSEBUTTONUP:
                bullet_table = player.shoot(bullet_table)

    def player_activity(self, player):
        """Moving and action for player"""
        player.move()
        player.draw(self.screen)

    def check_objects_status(self, bullet_table, alien_list, level_up_table, freeze_table, remove_table):
        """Action for objects"""
        for i in bullet_table:
            remove_table = i.check_status(self.screen, remove_table)

        for h in level_up_table:
            remove_table = h.check_status(self.screen, remove_table)

        for g in alien_list:
            remove_table = g.check_status(self.screen, remove_table, alien_list)

        for j in freeze_table:
            remove_table = j.check_status(self.screen, remove_table)

        self.remove_object(bullet_table, alien_list, level_up_table, freeze_table, remove_table)

    def play_game(self):
        self.prepare_music()
        self.play_music()
        player = Player(x_position=display.get_surface().get_width() / 2.1,
                        y_position=display.get_surface().get_height() / 1.2,
                        player_image=image.load("Spaceship_images\\SpaceShip_lev1.png"), level=1
                        )
        alien_list = self.make_aliens(self.game_level)
        bullet_table = []
        level_up_table = []
        freeze_table = []

        '''Main loop of the game'''
        while self.running:
            remove_table = []

            level_up_table, freeze_table = self.make_random_upgrades(level_up_table, freeze_table)

            alien_list, self.game_level, self.level_time = self.level_check(alien_list)
            self.loose_condition(player)

            '''Event loop'''
            self.check_events(player, bullet_table)

            '''Fill the background with space image'''
            self.make_background()

            self.check_objects_status(bullet_table, alien_list, level_up_table, freeze_table, remove_table)

            '''make player activities in the game'''
            self.player_activity(player)

            '''Checking collision for all objects'''
            self.collision_detection(alien_list, bullet_table, level_up_table, freeze_table, player)

            self.clock.tick(1000)

            if self.level_time:
                self.level_time = self.pause_screen("Level " + str(self.game_level), (255, 255, 255), self.level_time)

            '''Flip the display'''
            pygame.display.flip()
        pygame.quit()


space_invader = Game(game_level=1, level_time=time.get_ticks())
space_invader.play_game()
